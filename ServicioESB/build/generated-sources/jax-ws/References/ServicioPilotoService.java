
package References;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-1b01 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "ServicioPilotoService", targetNamespace = "http://WS/", wsdlLocation = "http://localhost:8091/ServicioPiloto/ServicioPiloto?wsdl")
public class ServicioPilotoService
    extends Service
{

    private final static URL SERVICIOPILOTOSERVICE_WSDL_LOCATION;
    private final static WebServiceException SERVICIOPILOTOSERVICE_EXCEPTION;
    private final static QName SERVICIOPILOTOSERVICE_QNAME = new QName("http://WS/", "ServicioPilotoService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8091/ServicioPiloto/ServicioPiloto?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SERVICIOPILOTOSERVICE_WSDL_LOCATION = url;
        SERVICIOPILOTOSERVICE_EXCEPTION = e;
    }

    public ServicioPilotoService() {
        super(__getWsdlLocation(), SERVICIOPILOTOSERVICE_QNAME);
    }

    public ServicioPilotoService(WebServiceFeature... features) {
        super(__getWsdlLocation(), SERVICIOPILOTOSERVICE_QNAME, features);
    }

    public ServicioPilotoService(URL wsdlLocation) {
        super(wsdlLocation, SERVICIOPILOTOSERVICE_QNAME);
    }

    public ServicioPilotoService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SERVICIOPILOTOSERVICE_QNAME, features);
    }

    public ServicioPilotoService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ServicioPilotoService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns ServicioPiloto
     */
    @WebEndpoint(name = "ServicioPilotoPort")
    public ServicioPiloto getServicioPilotoPort() {
        return super.getPort(new QName("http://WS/", "ServicioPilotoPort"), ServicioPiloto.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ServicioPiloto
     */
    @WebEndpoint(name = "ServicioPilotoPort")
    public ServicioPiloto getServicioPilotoPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://WS/", "ServicioPilotoPort"), ServicioPiloto.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SERVICIOPILOTOSERVICE_EXCEPTION!= null) {
            throw SERVICIOPILOTOSERVICE_EXCEPTION;
        }
        return SERVICIOPILOTOSERVICE_WSDL_LOCATION;
    }

}
