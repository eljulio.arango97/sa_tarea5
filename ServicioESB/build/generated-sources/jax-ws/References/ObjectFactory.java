
package References;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the References package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ObtenerPropuestaPiloto_QNAME = new QName("http://WS/", "ObtenerPropuestaPiloto");
    private final static QName _ObtenerPropuestaPilotoResponse_QNAME = new QName("http://WS/", "ObtenerPropuestaPilotoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: References
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObtenerPropuestaPilotoResponse }
     * 
     */
    public ObtenerPropuestaPilotoResponse createObtenerPropuestaPilotoResponse() {
        return new ObtenerPropuestaPilotoResponse();
    }

    /**
     * Create an instance of {@link ObtenerPropuestaPiloto }
     * 
     */
    public ObtenerPropuestaPiloto createObtenerPropuestaPiloto() {
        return new ObtenerPropuestaPiloto();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerPropuestaPiloto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "ObtenerPropuestaPiloto")
    public JAXBElement<ObtenerPropuestaPiloto> createObtenerPropuestaPiloto(ObtenerPropuestaPiloto value) {
        return new JAXBElement<ObtenerPropuestaPiloto>(_ObtenerPropuestaPiloto_QNAME, ObtenerPropuestaPiloto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerPropuestaPilotoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "ObtenerPropuestaPilotoResponse")
    public JAXBElement<ObtenerPropuestaPilotoResponse> createObtenerPropuestaPilotoResponse(ObtenerPropuestaPilotoResponse value) {
        return new JAXBElement<ObtenerPropuestaPilotoResponse>(_ObtenerPropuestaPilotoResponse_QNAME, ObtenerPropuestaPilotoResponse.class, null, value);
    }

}
