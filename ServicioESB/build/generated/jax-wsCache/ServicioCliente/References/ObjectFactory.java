
package References;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the References package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PeticionIni_QNAME = new QName("http://WS/", "PeticionIni");
    private final static QName _PeticionIniResponse_QNAME = new QName("http://WS/", "PeticionIniResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: References
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PeticionIni }
     * 
     */
    public PeticionIni createPeticionIni() {
        return new PeticionIni();
    }

    /**
     * Create an instance of {@link PeticionIniResponse }
     * 
     */
    public PeticionIniResponse createPeticionIniResponse() {
        return new PeticionIniResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionIni }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "PeticionIni")
    public JAXBElement<PeticionIni> createPeticionIni(PeticionIni value) {
        return new JAXBElement<PeticionIni>(_PeticionIni_QNAME, PeticionIni.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionIniResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "PeticionIniResponse")
    public JAXBElement<PeticionIniResponse> createPeticionIniResponse(PeticionIniResponse value) {
        return new JAXBElement<PeticionIniResponse>(_PeticionIniResponse_QNAME, PeticionIniResponse.class, null, value);
    }

}
