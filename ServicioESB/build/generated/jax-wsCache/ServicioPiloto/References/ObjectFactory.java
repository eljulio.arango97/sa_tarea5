
package References;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the References package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ObtenerConductoresDisponibles_QNAME = new QName("http://WS/", "ObtenerConductoresDisponibles");
    private final static QName _ObtenerConductoresDisponiblesResponse_QNAME = new QName("http://WS/", "ObtenerConductoresDisponiblesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: References
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObtenerConductoresDisponibles }
     * 
     */
    public ObtenerConductoresDisponibles createObtenerConductoresDisponibles() {
        return new ObtenerConductoresDisponibles();
    }

    /**
     * Create an instance of {@link ObtenerConductoresDisponiblesResponse }
     * 
     */
    public ObtenerConductoresDisponiblesResponse createObtenerConductoresDisponiblesResponse() {
        return new ObtenerConductoresDisponiblesResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerConductoresDisponibles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "ObtenerConductoresDisponibles")
    public JAXBElement<ObtenerConductoresDisponibles> createObtenerConductoresDisponibles(ObtenerConductoresDisponibles value) {
        return new JAXBElement<ObtenerConductoresDisponibles>(_ObtenerConductoresDisponibles_QNAME, ObtenerConductoresDisponibles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerConductoresDisponiblesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WS/", name = "ObtenerConductoresDisponiblesResponse")
    public JAXBElement<ObtenerConductoresDisponiblesResponse> createObtenerConductoresDisponiblesResponse(ObtenerConductoresDisponiblesResponse value) {
        return new JAXBElement<ObtenerConductoresDisponiblesResponse>(_ObtenerConductoresDisponiblesResponse_QNAME, ObtenerConductoresDisponiblesResponse.class, null, value);
    }

}
